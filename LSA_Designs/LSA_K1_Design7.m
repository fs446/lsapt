DesignCase = 'LSA_K1_Design7'
%WST-curved Array, 15HF, qHF=0.8467, WFS controlled, dBDD = -20, Kaiser win, betaKB=1
%[p.210,Sch16] & [p.216,Sch16] LSA #1
%[p.210,Sch16] & [p.220,Sch16] LSA #2
%[p.211,Sch16] & [p.224,Sch16] LSA #3
%[p.212,Sch16] & [p.228,Sch16] LSA #4
%[p.212,Sch16] & [p.232,Sch16] LSA #5
%[p.213,Sch16] & [p.236,Sch16] LSA #6
%[p.213,Sch16] & [p.240,Sch16] LSA #7
%[p.214,Sch16] & [p.244,Sch16] LSA #8

LSA.xRef = [60;10;0];
LSA.splay_deg = [+9;...   %# 1
    -0.5;...   %# 2
    -0.5;...   %# 3
    -0.5;...   %# 4
    -1;...   %# 5
    -1;...   %# 6
    -1;...  %# 7
    -1.5;...  %# 8
    -1;...  %# 9
    -2;...  %#10
    -2;...  %#11
    -2.5;...  %#12
    -3;...  %#13
    -3.5;...  %#14
    -4;...  %#15
    -4;...  %#16
    -5;...  %#17
    -5;...  %#18
    ]';

LSA.xT = [0;13.5;0];%position vector of top/1st LSA cabinet, when not tilted, in m
LSA.fPre = 100;     %prefilter freq in Hz

%HF Drivers:
%EAW Anya q = (1*2.54/100)/(0.425/14) = 0.8367

LSA.HF.q = 0.8467;       %q-Factor of HF, 
%closely matching EAW Anya but here with 15 LF drivers
%since our cabinet is slightly higher
%HF waveguide is then 1" like Anya

%LSA.HF.M = 1;
%LSA.HF.M = 3;       %number of pistons for HF
%LSA.HF.M = 11;       %number of pistons for HF
%LSA.HF.M = 14;       %number of pistons for HF
LSA.HF.M = 15;       %number of pistons for HF

LSA.betaKB = 1;

GraphicsParam.Plane.IsobarMax = 106+12;
GraphicsParam.Plane.IsobarMin = GraphicsParam.Plane.IsobarMax-24;
GraphicsParam.Plane.IsobarStep = 6;

%LSA.ControlType = 'Uniform';
LSA.ControlType = 'WFS';

%% WFS Control
xPS = [-7.5;+9.925;0];
PointSourceDirectivityFlag = 1;
dBmin = -100;
dBDD = -20;


%%
if LSA.HF.M==1
    LSA.HF.ElectricPower = 1*10^(+7/10); %applied electrical power in W (sine RMS norm) to get 120dBSPL@1m extrapolated from 16m (i.e. we get a flat cabinet)
elseif LSA.HF.M==3
    LSA.HF.ElectricPower = 1*10^(-2.542/10); %applied electrical power in W (sine RMS norm) to get 120dBSPL@1m extrapolated from 16m (i.e. we get a flat cabinet)
elseif LSA.HF.M==11
    LSA.HF.ElectricPower = 1*10^(-2.542/10)*10^(-11.3/10);
elseif LSA.HF.M==14
    LSA.HF.ElectricPower = 1*10^(-15.9/10);
elseif LSA.HF.M==15
    LSA.HF.ElectricPower = 1*10^(-16.5/10);    
end