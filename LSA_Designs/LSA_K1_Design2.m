DesignCase = 'LSA_K1_Design2'
%Arc LSA, 3HF, qHF=1, uniformly driven, Kaiser win, betaKB = 3
%[p.210,Sch16] & [p.216,Sch16] LSA #1
%[p.210,Sch16] & [p.220,Sch16] LSA #2
%[p.211,Sch16] & [p.224,Sch16] LSA #3
%[p.212,Sch16] & [p.228,Sch16] LSA #4
%[p.212,Sch16] & [p.232,Sch16] LSA #5
%[p.213,Sch16] & [p.236,Sch16] LSA #6
%[p.213,Sch16] & [p.240,Sch16] LSA #7
%[p.214,Sch16] & [p.244,Sch16] LSA #8

LSA.xRef = [60;10;0];  %not used
LSA.splay_deg = [+9.75;...   %# 1
    -2.5;...   %# 2
    -2.5;...   %# 3
    -2.5;...   %# 4
    -2.5;...   %# 5
    -2.5;...   %# 6
    -2.5;...  %# 7
    -2.5;...  %# 8
    -2.5;...  %# 9
    -2.5;...  %#10
    -2.5;...  %#11
    -2.5;...  %#12
    -2.5;...  %#13
    -2.5;...  %#14
    -2.5;...  %#15
    -2.5;...  %#16
    -2.5;...  %#17
    -2.5;...  %#18
    ]';

LSA.xT = [0;13.5;0];%position vector of top/1st LSA cabinet, when not tilted, in m
LSA.fPre = 50;     %prefilter freq in Hz

%HF Drivers:
LSA.HF.q = 1;       %q-Factor of HF

%LSA.HF.M = 1;
LSA.HF.M = 3;       %number of pistons for HF
%LSA.HF.M = 11;       %number of pistons for HF
%LSA.HF.M = 14;       %number of pistons for HF
%LSA.HF.M = 15;       %number of pistons for HF

LSA.betaKB = 3;

GraphicsParam.Plane.IsobarMax = 106+12;
GraphicsParam.Plane.IsobarMin = GraphicsParam.Plane.IsobarMax-30;
GraphicsParam.Plane.IsobarStep = 6;

LSA.ControlType = 'Uniform';

%%
if LSA.HF.M==1
    LSA.HF.ElectricPower = 1*10^(+7/10); %applied electrical power in W (sine RMS norm) to get 120dBSPL@1m extrapolated from 16m (i.e. we get a flat cabinet)
elseif LSA.HF.M==3
    LSA.HF.ElectricPower = 1*10^(-2.542/10); %applied electrical power in W (sine RMS norm) to get 120dBSPL@1m extrapolated from 16m (i.e. we get a flat cabinet)
elseif LSA.HF.M==11
    LSA.HF.ElectricPower = 1*10^(-2.542/10)*10^(-11.3/10);
elseif LSA.HF.M==14
    LSA.HF.ElectricPower = 1*10^(-15.9/10);
elseif LSA.HF.M==15
    LSA.HF.ElectricPower = 1*10^(-16.5/10);    
end