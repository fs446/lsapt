%% *****************************************
% approximation of an L'Acoustics K1 Cabinet
% in terms of power and radiation characteristics
% note that the piston model differs from reality
% and the real waveguide is different from the here modeled
% line piston
% So: This provides not the same prediction results as SoundVision would
% do!!! However, with the chosen parameters you come pretty close.
% ******************************************
% Re-engineering with SoundVision 3.0.1
% Transducers:
% HF: 3 x 3" compression drivers with
% DOSC waveguides (single waveguide or 3 individual waveguides not known)
% MF: 4 x 6.5" direct-radiating
% LF: 2 x 15" bass-reflex
% Nominal impedance & RMS power handling
% LF section: 2 x 8 Ohm, 2 x 600 W
% MF section: 8 Ohm, 600 W
% HF section: 8 Ohm, 220 W
% each section acts as a nominal 8 Ohm

% in SoundVision for LF we get 110 dB SPL at 16m with full possible gain
% without overload
% this yields 20*log10(16)+110 dBSPL=134.0824 dBSPL for 1m when assuming
% 6dB/distance doubling
% typical 15" drivers have something like 98dBSPL 1W,1m sensitivities
% since two 15" operate together we assume a 6dB SPL gain when simulating
% only a line contour as LSA
% thus (98+6)dBSPL 1W,1m sensitivity is assumed
% we assume that each driver can handle 600 W RMS->so all together 1200W
% can be applied, this makes a maximum gain of 10*log10(1200/1) = 30.7918 dB
% w.r.t. driving LF with nominal sensitivity of 1W
% max SPL is obtained when  10*log10(1200)+104 = 134.7918 dB SPL max

% in SoundVision for MF we get between approx 113-123 dB SPL at 16m with
% full possible gain
% we should model this with the following assumptions
% take http://www.bcspeakers.com/products/lf-driver/6-5/6mbx44
% B&C 6MBX44
% Nominal power handling 150 W
% Sensitivity (1W/1m) *	98.0 dB
% this corresponds well with the 4x150W=600W K1 spec sheet
% since we really simulate two drivers on the line cotour in the LSA,
% we only have to compensate for the
% other two that are symmetrical w.r.t x-axis
% so 1 LSA piston here is equivalent to 2x6,5" drivers in the cabinet
% therefore use a sens of 98+6=104 dBSPL 1W,1m for the LSA piston
% and assume max power handling of 300 W (i.e. 2*150W)
% we then get about the same 134.8 dBSPL max as for the LF
% 10*log10(300)+104+6 = 134.77 dB

% for HF a 113dB SPL 1W,1m sensitivity for each compression driver is
% reasonable with max power of 75 W
% this yields comparable results with SoundVision
%***********************

%cf. [Table 4.3,Sch16]

LSA.dy0 = 0.45;       %height of one LSA cabinet in m->K1 with small gap

LSA.fXOLM = 350;    %crossover freq LF / MF in Hz
LSA.fXOMH = 1300;   %crossover freq MF / HF in Hz
LSA.fHP = 68;       %highpass freq in Hz
LSA.NHP = 6;        %Butterworth order highpass
LSA.fLP = 13000;    %lowpass freq in Hz
LSA.NLP = 6;        %Butterworth order lowpass

LSA.PreFilterFlag = 1; %apply LowShelve Compensation for Prefilter

%LF Drivers:
LSA.LF.M = 1;       %number of pistons for LF
LSA.LF.PistonModel = 'Circ';    %piston model LF
LSA.LF.q = (15*2.54/100)/(LSA.dy0/LSA.LF.M);  %q-Factor of LF
LSA.LF.Sensitivity = 98+6; %dBSPL @ 1W,1m LF
LSA.LF.MaxElectricPower = 1200; %max applicable power in W
LSA.LF.ElectricPower = 1*10^(16/10);  %applied electrical power in W (sine RMS norm) to get 120dBSPL@1m extrapolated from 16m (i.e. we get a flat cabinet)

%MF Drivers:
LSA.MF.M = 2;       %number of pistons for MF
LSA.MF.PistonModel = 'Circ';    %piston model MF
LSA.MF.q = (6.5*2.54/100)/(LSA.dy0/LSA.MF.M); %q-Factor of MF
LSA.MF.Sensitivity = 98+6; %dBSPL @ 1W,1m MF
LSA.MF.MaxElectricPower = 300; %max applicable power in W
LSA.MF.ElectricPower = 1*10^(9.98/10); %applied electrical power in W (sine RMS norm) to get 120dBSPL@1m extrapolated from 16m (i.e. we get a flat cabinet)

%HF Drivers:
LSA.HF.PistonModel = 'Line';    %piston model HF
LSA.HF.Sensitivity = 113;    %dBSPL @ 1W,1m HF
LSA.HF.MaxElectricPower = 75;