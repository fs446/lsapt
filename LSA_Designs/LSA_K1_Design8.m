DesignCase = 'LSA_K1_Design8'
%Straight Array, 15HF, qHF=0.8467, WFS controlled, dBDD = -20, Kaiser win, betaKB=1
%[p.210,Sch16] & [p.216,Sch16] LSA #1
%[p.210,Sch16] & [p.220,Sch16] LSA #2
%[p.211,Sch16] & [p.224,Sch16] LSA #3
%[p.212,Sch16] & [p.228,Sch16] LSA #4
%[p.212,Sch16] & [p.232,Sch16] LSA #5
%[p.213,Sch16] & [p.236,Sch16] LSA #6
%[p.213,Sch16] & [p.240,Sch16] LSA #7
%[p.214,Sch16] & [p.244,Sch16] LSA #8

LSA.xRef = [60;10;0];
LSA.splay_deg = [+0;...   %# 1
    0;...   %# 2
    0;...   %# 3
    0;...   %# 4
    0;...   %# 5
    0;...   %# 6
    0;...  %# 7
    0;...  %# 8
    0;...  %# 9
    0;...  %#10
    0;...  %#11
    0;...  %#12
    0;...  %#13
    0;...  %#14
    0;...  %#15
    0;...  %#16
    0;...  %#17
    0;...  %#18
    ]';

LSA.xT = [0;13.5;0];%position vector of top/1st LSA cabinet, when not tilted, in m
LSA.fPre = 150;     %prefilter freq in Hz

%HF Drivers:
%EAW Anya q = (1*2.54/100)/(0.425/14) = 0.8367

LSA.HF.q = 0.8467;       %q-Factor of HF, 
%closely matching EAW Anya but here with 15 LF drivers
%since our cabinet is slightly higher
%HF waveguide is then 1" like Anya

%LSA.HF.M = 1;
%LSA.HF.M = 3;       %number of pistons for HF
%LSA.HF.M = 11;       %number of pistons for HF
%LSA.HF.M = 14;       %number of pistons for HF
LSA.HF.M = 15;       %number of pistons for HF

LSA.betaKB = 1;

GraphicsParam.Plane.IsobarMax = 106+12;
GraphicsParam.Plane.IsobarMin = GraphicsParam.Plane.IsobarMax-24;
GraphicsParam.Plane.IsobarStep = 6;

LSA.ControlType = 'WFS';

%% WFS Control
xPS = [-7.5;+9.925;0];
PointSourceDirectivityFlag = 1;
dBmin = -100;
dBDD = -20;

%%
if LSA.HF.M==1
    LSA.HF.ElectricPower = 1*10^(+7/10); %applied electrical power in W (sine RMS norm) to get 120dBSPL@1m extrapolated from 16m (i.e. we get a flat cabinet)
elseif LSA.HF.M==3
    LSA.HF.ElectricPower = 1*10^(-2.542/10); %applied electrical power in W (sine RMS norm) to get 120dBSPL@1m extrapolated from 16m (i.e. we get a flat cabinet)
elseif LSA.HF.M==11
    LSA.HF.ElectricPower = 1*10^(-2.542/10)*10^(-11.3/10);
elseif LSA.HF.M==14
    LSA.HF.ElectricPower = 1*10^(-15.9/10);
elseif LSA.HF.M==15
    LSA.HF.ElectricPower = 1*10^(-16.5/10);    
end