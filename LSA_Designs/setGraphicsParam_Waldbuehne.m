GraphicsParam.VenueLSA.xmin = -10; %m
GraphicsParam.VenueLSA.dx = 20; %m
GraphicsParam.VenueLSA.xmax = 120; %m
GraphicsParam.VenueLSA.ymin = -10; %m
GraphicsParam.VenueLSA.dy = 10; %m
GraphicsParam.VenueLSA.ymax = +40; %m

GraphicsParam.SingleCabinet.ymin = 104; %dB SPL
GraphicsParam.SingleCabinet.dy = 6; %dB
GraphicsParam.SingleCabinet.ymax = 140; %dB SPL
GraphicsParam.SingleCabinet.CMType = CMType;
GraphicsParam.SingleCabinet.CM = CM;
GraphicsParam.SingleCabinet.CMFlip = 0;

GraphicsParam.SingleCabinetFRP.Max_dB = 96; %dB SPL
GraphicsParam.SingleCabinetFRP.Step_dB = 1; %dB
GraphicsParam.SingleCabinetFRP.Min_dB = 36; %dB SPL
GraphicsParam.SingleCabinetFRP.ddB = 10; %dB
GraphicsParam.SingleCabinetFRP.CMType = CMType; %cbrewer style
GraphicsParam.SingleCabinetFRP.CM = CM;
GraphicsParam.SingleCabinetFRP.CMFlip = 1;
GraphicsParam.SingleCabinetFRP.ymin = -75; %deg
GraphicsParam.SingleCabinetFRP.dy = 15; %deg
GraphicsParam.SingleCabinetFRP.ymax = 75; %deg

GraphicsParam.DFIP.Max_dB = 30; %dB SPL
GraphicsParam.DFIP.Step_dB = 3; %dB
GraphicsParam.DFIP.Min_dB = -30; %dB SPL
GraphicsParam.DFIP.ddB = 6; %dB
GraphicsParam.DFIP.CMType = CMType; %cbrewer style
GraphicsParam.DFIP.CM = CM;
GraphicsParam.DFIP.CMFlip = 1;

GraphicsParam.Line.ymin = 88; %dB SPL
GraphicsParam.Line.dy = 6; %dB
GraphicsParam.Line.ymax = 118; %dB SPL
GraphicsParam.Line.CMType = CMType;
GraphicsParam.Line.CM = CM;
GraphicsParam.Line.CMFlip = 1;

GraphicsParam.PIP.Max_dB = 118; %dB SPL
GraphicsParam.PIP.Step_dB = 1; %dB
GraphicsParam.PIP.Min_dB = 94; %dB SPL
GraphicsParam.PIP.ddB = 6; %dB
GraphicsParam.PIP.CMType = CMType; %cbrewer style
GraphicsParam.PIP.CM = CM;
GraphicsParam.PIP.CMFlip = 1;

GraphicsParam.IR.CMType = CMType;
GraphicsParam.IR.CM = CM;
GraphicsParam.IR.xmin = 0; %ms
GraphicsParam.IR.dx = 100; %ms
GraphicsParam.IR.xmax = 400; %ms
GraphicsParam.IR.ymin = -60; %dB
GraphicsParam.IR.dy = 6; %dB
GraphicsParam.IR.ymax = 0; %dB
GraphicsParam.IR.CMFlip = 1;
GraphicsParam.IR.Max_dB = 0; %dB SPL
GraphicsParam.IR.Step_dB = 1; %dB
GraphicsParam.IR.Min_dB = -100; %dB SPL
GraphicsParam.IR.ddB = 10; %dB


GraphicsParam.Plane.xmin = 0; %m
GraphicsParam.Plane.dx = 20; %m
GraphicsParam.Plane.xmax = 120; %m
GraphicsParam.Plane.ymin = -5; %m
GraphicsParam.Plane.dy = 10; %m
GraphicsParam.Plane.ymax = +45; %m
GraphicsParam.Plane.Max_dB = 118; %dB SPL
GraphicsParam.Plane.Step_dB = 1; %dB
GraphicsParam.Plane.Min_dB = 94; %dB SPL
GraphicsParam.Plane.ddB = 6; %dB
GraphicsParam.Plane.CMType = CMType; %cbrewer style
GraphicsParam.Plane.CM = CM;
GraphicsParam.Plane.CMFlip = 1;
GraphicsParam.Plane.IsobarFlag = 1;
GraphicsParam.Plane.IsobarMax = 118;
GraphicsParam.Plane.IsobarMin = 88;
GraphicsParam.Plane.IsobarStep = 6;

GraphicsParam.FRP.Max_dB = 130; %dB SPL
GraphicsParam.FRP.Step_dB = 1; %dB
GraphicsParam.FRP.Min_dB = 80; %dB SPL
GraphicsParam.FRP.ddB = 10; %dB
GraphicsParam.FRP.CMType = CMType; %cbrewer style
GraphicsParam.FRP.CM = CM;
GraphicsParam.FRP.CMFlip = 1;
GraphicsParam.FRP.ymin = -75; %deg
GraphicsParam.FRP.dy = 15; %deg
GraphicsParam.FRP.ymax = 75; %deg

GraphicsParam.PLineDistribution.ymin = 88;
GraphicsParam.PLineDistribution.dy = 3;
GraphicsParam.PLineDistribution.ymax = 118;
GraphicsParam.PLineDistribution.CMType = CMType; %cbrewer style
GraphicsParam.PLineDistribution.CM = CM;


