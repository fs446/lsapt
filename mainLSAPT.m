% ************************************
% Line Source Array Prediction Toolbox
% ************************************
% initiated by Frank Schultz, scf175@googlemail.com
% https://orcid.org/0000-0002-3010-0294
% http://spatialaudio.net/author/frank-schultz/
% https://www.researchgate.net/profile/Frank_Schultz
% 
% hosted under: https://fs446@bitbucket.org/fs446/lsapt.git
%
% note that the used figure handling does not work for Matlab releases
% > Matlab R2013b
% Tag v0.06/v0.07 : renders graphics of the last chapter of
% [Sch16] Frank Schultz (2016): "Sound Field Synthesis for Line Source Array
% Applications in Large-Scale Sound Reinforcement." doctoral thesis, 
% University of Rostock, official print for release version: 2016-06-13
%
% This project should be continued in Python... 

run initLSAPT;
tstart = tic;

run setGraphicsParam_Waldbuehne;    %set ticks, tick labels, colorbar etc.

setup_and_check_SingleCabinet = 0;  %1...for setting a new LSA cabinet towards a flat (or whatever) freq resp
                                    %0...do the normal LSA prediction
setup_and_check_SingleCabinet_FRPFlag = 1; %plot FRP of the single cabinet

%%
SimRes.PlaneFlag = 1;   %take xy-plane into account for calc
SimRes.AudFlag = 1;     %take audience positions into account for calc
SimRes.AvoidFlag = 0;   %take avoid positions into account for calc
SimRes.FRPFlag = 1;     %take FRP calc into account
SimRes.CalcPreATF = 1;  %calc PreATF = 1, or load PreATF from file = 0

SimRes.df = 10;         %Hz, freq resolution for DFT
SimRes.dxPlane = 1;     %m, x-space resolution for xy-Plane
SimRes.dyPlane = 1;     %m, y-space resolution for xy-Plane
SimRes.dphi_degFRP = 5; %deg, FRP angular resolution
SimRes.dxAud = 1;       %m, space resolution along audience line
SimRes.dxAvoid = 1;     %m, space resolution along avoid line

%%
%****************
%Acoustics Setup:
%****************
Ac.c = getSpeedofSound(); %in m/s
Ac.fs = getSamplingFrequency(); %in Hz
Ac.df = SimRes.df; %DFT resolution in Hz
Ac.f = getFrequencies(Ac.df, Ac.fs); %DFT frequency vector
Ac.p0 = 20 * 10^(-6);  %Ref-SPL in Pa
Ac.Pref = 1; %power in W
Ac.fdes = [125 250 500 630 1000 2000 3150 4000 5000 6300 8000 10000]; %Hz

%%
%*****************
%Polar Plot Setup:
%*****************
Venue.FRP.xM = [0;0;0]; %this will be overwritten in LSA Designs, assumed origin behind the LSA for FRP calc
Venue.FRP.rFar = 2^14; %very far away :-)
Venue.FRP.dphi_deg = SimRes.dphi_degFRP;
%%
%************
%Venue Setup:
%************
run Venue_Waldbuehne;
Venue = setVenue(Venue); %init Venue
%% LSA Design Studies:
run LSA_K1_like; %use but don't change this script for the following PhD LSA design studies

%use either on of the [Ch. 4,Sch16] LSA design studies:
%run LSA_K1_Design1; %Straight LSA, 3HF, qHF=0.8467, uniformly driven, not windowed
%run LSA_K1_Design2; %Arc LSA, 3HF, qHF=1, uniformly driven, Kaiser win, betaKB = 3
%run LSA_K1_Design3; %WST-Curved LSA, 3HF, qHF=1, uniformly driven, Kaiser win, betaKB=2
run LSA_K1_Design4; %WST-Curved LSA, 3HF, q=0.8467, uniformly driven, Kaiser win, betaKB=2
%run LSA_K1_Design5; %WST-curved Array, 15HF, qHF=0.8467, WFS controlled, dBDD = -10, Kaiser win, betaKB=1
%run LSA_K1_Design6; %WST-curved Array, 15HF, qHF=0.8467, WFS controlled, dBDD = 0, Kaiser win, betaKB=2
%run LSA_K1_Design7; %WST-curved Array, 15HF, qHF=0.8467, WFS controlled, dBDD = -20, Kaiser win, betaKB=1
%run LSA_K1_Design8; %Straight Array, 15HF, qHF=0.8467, WFS controlled, dBDD = -20, Kaiser win, betaKB=1

%%
LSA = setLSA(LSA,Ac); %init LSA

%get the origin position w.r.t. the FRP is calculated:
xOr_FRP = LSA.HF.x0(:,1)-LSA.HF.x0(:,end);
xOr_FRP = LSA.HF.x0(:,1) - xOr_FRP/norm(xOr_FRP)*norm(xOr_FRP)/2;
Venue.FRP.xM = xOr_FRP;
clear xOr_FRP;

%% Check Single LSA Cabinet for Flat Freq Resp
checkSingleCabinet(fh2,fh6,LSA,Ac,GraphicsParam.SingleCabinet,GraphicsParam.SingleCabinetFRP,setup_and_check_SingleCabinet_FRPFlag)

if ~setup_and_check_SingleCabinet
    %% get all PreATFs for the chosen LSA / Venue Combination
    run calcPreATFs_for_Venue_with_LSA;
    
    %% %%%%%%%%%%%%
    % LSA Control:
    %%%%%%%%%%%%%%%
    if strcmp(LSA.ControlType,'Uniform')
        run setUniformLSA;
    elseif strcmp(LSA.ControlType,'WFS')
        [LSA.LF.D, LSA.MF.D, LSA.HF.D,...
            DlyLF, DlyMF, DlyHF,...
            phi_deg, G_dB,...
            phiPS_LSA_deg_LF, GainPS_LSA_dB_LF,...
            phiPS_LSA_deg_MF, GainPS_LSA_dB_MF,...
            phiPS_LSA_deg_HF, GainPS_LSA_dB_HF] = ...
            getDrivingFunctionWFS_DirectedPointSource(...
            LSA.LF.x0,LSA.LF.unitn,...
            LSA.MF.x0,LSA.MF.unitn,...
            LSA.HF.x0,LSA.HF.unitn,...
            Venue.Aud.x, xPS, LSA.xRef,...
            dBmin, dBDD,...
            Ac.f,Ac.c,PointSourceDirectivityFlag);
    else
        %here other LSA control approaches might be added
    end
    run setFinalDriving_XO_Win_Gain;
    
    %% plot the venue slice
%     plotVenue(fh1,Venue.Aud.x)
%     plotLSA(fh1,LSA.LF.x0,LSA.LF.unitn,LSA.dy0)
%     plotLSA(fh1,LSA.LF.x0,LSA.LF.unitn,LSA.LF.PistonDimension*2)
%     plotLSA(fh1,LSA.MF.x0,LSA.MF.unitn,LSA.MF.PistonDimension*2)
%     plotLSA(fh1,LSA.HF.x0,LSA.HF.unitn,LSA.HF.PistonDimension)
    %e.g. [Fig. 4.14, Ch16]
    plotVenueLSA(fh1,Venue.Aud.x,LSA.LF.x0,LSA.LF.unitn,LSA.dy0,GraphicsParam.VenueLSA)
    figure(fh1)
    hold on
    if strcmp(LSA.ControlType,'WFS')
        plot(xPS(1),xPS(2),'ok','MarkerSize',2,'MarkerfaceColor','k')
        plot(LSA.xRef(1),LSA.xRef(2),'ok','MarkerSize',2,'MarkerfaceColor','k')
    end
    plot(Venue.FRP.xM(1),Venue.FRP.xM(2),'or','MarkerSize',1.5,'MarkerfaceColor','r')
    %plot position indices, hard coded for 4 stands:
    tmp = floor(size(Venue.Aud.x,2)/4);
    tmp = [1:tmp:4*tmp size(Venue.Aud.x,2)];
    for n=1:length(tmp)
        text(Venue.Aud.x(1,tmp(n)),Venue.Aud.x(2,tmp(n))-3,[num2str(tmp(n))],'HorizontalAlignment','center')
    end
    hold off
    %% plot DFIPs, eq. [Fig. 4.15 (a)-(c)]
    plotDFIP(fh7, Ac.f, LSA.LF.D, LSA.LF.Sensitivity, LSA.LF.MaxElectricPower,LSA.LF.M,Ac.p0, Ac.Pref, GraphicsParam.DFIP)
    plotDFIP(fh8, Ac.f, LSA.MF.D, LSA.MF.Sensitivity, LSA.MF.MaxElectricPower,LSA.MF.M,Ac.p0, Ac.Pref, GraphicsParam.DFIP)
    plotDFIP(fh9, Ac.f, LSA.HF.D, LSA.HF.Sensitivity, LSA.HF.MaxElectricPower,LSA.HF.M,Ac.p0, Ac.Pref, GraphicsParam.DFIP)
    %% get LB1-Measure, eq. [Fig. 4.15 (d)]
    fStart = LSA.fHP; fStop = LSA.fXOLM;
    [fLB1_LF, LB1_LF] = getLB1(LSA.LF.D,LSA.LF.Sensitivity,Ac.f,fStart,fStop,Ac.Pref,Ac.p0);
    fStart = LSA.fXOLM; fStop = LSA.fXOMH;
    [fLB1_MF, LB1_MF] = getLB1(LSA.MF.D,LSA.MF.Sensitivity,Ac.f,fStart,fStop,Ac.Pref,Ac.p0);
    fStart = LSA.fXOMH; fStop = LSA.fLP;
    [fLB1_HF, LB1_HF] = getLB1(LSA.HF.D,LSA.HF.Sensitivity,Ac.f,fStart,fStop,Ac.Pref,Ac.p0);
    plotLB1(fh15,...
        fLB1_LF,LB1_LF,...
        fLB1_MF,LB1_MF,...
        fLB1_HF,LB1_HF,...
        LSA.LF.MaxElectricPower,LSA.MF.MaxElectricPower,LSA.HF.MaxElectricPower,...
        Ac.Pref,GraphicsParam.PLineDistribution);
    %% get LB2-Measure, eq. [Fig. 4.15 (e)]
    fStart = LSA.fHP; fStop = LSA.fXOLM;
    [LB2_LF] = getLB2(LSA.LF.D,LSA.LF.Sensitivity,Ac.f,fStart,fStop,Ac.Pref,Ac.p0);
    fStart = LSA.fXOLM; fStop = LSA.fXOMH;
    [LB2_MF] = getLB2(LSA.MF.D,LSA.MF.Sensitivity,Ac.f,fStart,fStop,Ac.Pref,Ac.p0);
    fStart = LSA.fXOMH; fStop = LSA.fLP;
    [LB2_HF] = getLB2(LSA.HF.D,LSA.HF.Sensitivity,Ac.f,fStart,fStop,Ac.Pref,Ac.p0);
    plotLB2(fh16,LB2_LF,LB2_MF,LB2_HF,LSA.LF.M,LSA.MF.M,LSA.HF.M,GraphicsParam.PLineDistribution)
    %% Solo / Inverted Cabinet
    %SoloNumberOfCabinet=13;
    %[LSA.LF.D,LSA.MF.D,LSA.HF.D] = getSoloDrivenCabinet(LSA.LF.D,LSA.MF.D,LSA.HF.D,LSA.LF.M,LSA.MF.M,LSA.HF.M,SoloNumberOfCabinet);
    %InvertedNumberOfCabinet=6;
    %[LSA.LF.D,LSA.MF.D,LSA.HF.D] = getInvertedDrivenCabinet(LSA.LF.D,LSA.MF.D,LSA.HF.D,LSA.LF.M,LSA.MF.M,LSA.HF.M,InvertedNumberOfCabinet);
    
    %% SFS Kernel Calc & Plot [eq. 4.29, Sch16], e.g. [Fig. 4.16 (a)-(d), Sch16], [Fig. 4.17, Sch16]
    if SimRes.PlaneFlag %Plane
        tic
        [PPlane, feval] = getPPlane(Pred.Plane, LSA, Venue.Plane.N1, Venue.Plane.N2, Ac.c, Ac.f, Ac.fdes);
        plotPPlane(fh3, fhCBSPLxy, Venue.Plane.xPl, Venue.Plane.yPl, Venue.Aud.x, feval, PPlane, Ac.p0, GraphicsParam.Plane);
        disp('SFS Plane:')
        toc
    end
    if SimRes.AudFlag %Audience
        tic
        PAud = getPLine(Pred.Aud, LSA, Ac.c, Ac.f);
        plotPLine(fh4, Ac.f, PAud, Ac.p0, GraphicsParam.Line);
        plotPIP(fh11, Ac.f, PAud,Ac.p0, GraphicsParam.PIP);
        [pIR, Ac.t] = getpIR(PAud, Ac.fs);
        plotpIR(fh5,Ac.t, pIR, GraphicsParam.IR);
        %plotpIRsurf(fh5,Ac.t, pIR, GraphicsParam.IR);
        disp('SFS Aud:')
        toc
    end
%     if SimRes.AvoidFlag %Avoid
%         tic
%         PAvoid = getPLine(Pred.Avoid, LSA, Ac.c, Ac.f);
%         plotPLine(fhCBSPLxy, Ac.f, PAvoid, Ac.p0, GraphicsParam.Line);
%         disp('SFS Avoid:')
%         toc
%     end
    if SimRes.FRPFlag %FRP
        tic
        PFRP = getPFRP(Pred.FRP, LSA, Ac.c, Ac.f);
        plotFRP(fh6, Ac.f, Venue.FRP.phi, PFRP,Ac.p0, Venue.FRP.rFar, GraphicsParam.FRP)
        disp('SFS FRP:')
        toc
    end
    disp('whole processing:')
    toc(tstart)
    
    %% Quality Calc & Plot, e.g. [Fig. 4.16 (e), Sch16]
    if SimRes.AudFlag
        fL = 200;
        fH = 5000;
        PAudDistribution = getPLineDistribution(PAud, Ac.f, fL, fH, Ac.p0);
        plotPLineDistribution(fh10, PAudDistribution, GraphicsParam.PLineDistribution, fL, fH)
    end
%     %% SPL White / Pink Noise
%     if 0
%         run getSPL;
%     end
    
    if strcmp(LSA.ControlType,'WFS')
        %% check Spline Interp:
        figure(fh12)
        subplot(3,1,1)
        plot(phi_deg,G_dB,'k','LineWidth',2), hold on
        plot(phiPS_LSA_deg_LF,GainPS_LSA_dB_LF,'oc','LineWidth',1,'MarkerSize',1.5)
        plot(phiPS_LSA_deg_LF,GainPS_LSA_dB_LF,'c','LineWidth',1)
        hold off
        xlabel('\phi / deg')
        ylabel('A_{FRP,PS} / dB')
        grid on
        set(gca,'XTick',[-90:15:+90])
        axis([-90 +90 max(GainPS_LSA_dB_LF)-40 max(GainPS_LSA_dB_LF)+10])
        title('LF')
        subplot(3,1,2)
        plot(phi_deg,G_dB,'k','LineWidth',2), hold on
        plot(phiPS_LSA_deg_MF,GainPS_LSA_dB_MF,'oc','LineWidth',1,'MarkerSize',1.5)
        plot(phiPS_LSA_deg_MF,GainPS_LSA_dB_MF,'c','LineWidth',1),
        hold off
        xlabel('\phi / deg')
        ylabel('A_{FRP,PS} / dB')
        grid on
        set(gca,'XTick',[-90:15:+90])
        axis([-90 +90 max(GainPS_LSA_dB_MF)-40 max(GainPS_LSA_dB_MF)+10])
        title('MF')
        subplot(3,1,3)
        plot(phi_deg,G_dB,'k','LineWidth',2), hold on
        plot(phiPS_LSA_deg_HF,GainPS_LSA_dB_HF,'oc','LineWidth',1,'MarkerSize',1.5)
        plot(phiPS_LSA_deg_HF,GainPS_LSA_dB_HF,'c','LineWidth',1),
        hold off
        xlabel('\phi / deg')
        ylabel('A_{FRP,PS} / dB')
        grid on
        set(gca,'XTick',[-90:15:+90])
        axis([-90 +90 max(GainPS_LSA_dB_HF)-40 max(GainPS_LSA_dB_HF)+10])
        title('HF')
        %% plot polar of the virtual PS FRP, e.g. [Fig. 4.42 (b), Sch16]
        figure(fh13) 
        Polar_dBStep = 6;
        maxPol = ceil(max(G_dB)/Polar_dBStep)*Polar_dBStep;
        minPol = maxPol-Polar_dBStep*4;
        PolarTicks=[minPol:Polar_dBStep:maxPol];
        hPolar = mmpolar(phi_deg*pi/180,G_dB,'k',...
            'FontSize',8,...
            'FontName','Arial',...
            'RLimit',[PolarTicks(1) PolarTicks(end)],...
            'TZeroDirection','East',...
            'RTickValue',PolarTicks,...
            'RTickAngle',0.001,...
            'RTickOffset', 0.025,...
            'TTickDirection','out',...
            'TTickDelta',15,...
            'RTickUnits','dB',...
            'RTickLabelVisible','off',...
            'RGridLineStyle','-',...
            'TGridLineStyle','-',...
            'TGridLineWidth',0.25,...
            'RGridLineWidth',0.25,...
            'TTickOffset',0.115);
        set(hPolar,'LineWidth',2)
        title([num2str(Polar_dBStep),'dB/div'])
        %% plots WFS Delays:, e.g. [Fig. 4.42 (c), Sch16]
        figure(fh14)
        cm = cbrewer('seq', 'YlGnBu', 50);
        plot(DlyHF*1000,[1:size(DlyHF,1)]/LSA.HF.M, 'o','MarkerSize',1,'Color',cm(30,:),'MarkerFaceColor',cm(10,:)), hold on
        plot(DlyMF*1000,[1:size(DlyMF,1)]/LSA.MF.M,'o','MarkerSize',2,'Color',cm(40,:),'MarkerFaceColor',cm(20,:))
        plot(DlyLF*1000,[1:size(DlyLF,1)]/LSA.LF.M,'o','MarkerSize',3,'Color',cm(50,:),'MarkerFaceColor',cm(40,:))
        hold off
        axis([0 2 0 18])
        ylabel('LSA cabinet')
        xlabel('\tau / ms')
        set(gca,'YTick',[0:2:LSA.N])
        set(gca,'XTick',[0:0.25:2])
        set(gca,'YDir','reverse')
        grid on
        legend('HF','MF','LF','Location','SouthEast')
        
    else
        figure(fh12)
        clf;
        figure(fh13)
        clf;
        figure(fh14)
        clf;
    end
end

%not used in [Sch16]
% %% Acoustic Contrast Audience vs. Avoid:
% if 0
%     figure(fhCBSPLxy)
%     Lp_a_na = 10*log10(...
%         (sum(abs(PAud).^2,2)/size(PAud,2)) ./ (sum(abs(PAvoid).^2,2)/size(PAvoid,2)));
%     semilogx(Ac.f,Lp_a_na)
%     axis([200 20000 0 20])
%     xlabel('f / Hz')
%     ylabel('L_{p,a,na} / dB')
%     grid on
% end
% %% Directivity Index (not used in PhD)
% if 0
%     H0 = abs(PFRP(find(Venue.FRP.phi>0*pi/180,1,'first'),:)).^2;
%     HN = sum(abs(PFRP).^2,1)/size(PFRP,1);
%     DI = 10*log10(H0./HN); %direcitivty index, checked with a monopole:
%     % Note to the DI !!!:
%     %ATF = exp(-1i*w_c*r)./(4*pi*r) -> gives exactly 0dB!
%     %Kirchhoff-Diffraction:
%     %ATF = 0.5*(1+cos(beta)).*exp(-1i*w_c*r)./(4*pi*r); -> 1.6dB!
%     %so an DI calculated  from Kirchhoff-Diffraction estimates a
%     %more directed source by 1.6dB
%     %you may want to compensate this or even not
%     %if assuming that %Kirchhoff-Diffraction is more precise for curved arrays
%     %in larger radiation angles!!!
%     figure(fh4)
%     semilogx(Ac.f,DI)
% end















