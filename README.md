Line Source Array Prediction Toolbox

initiated by Frank Schultz, https://orcid.org/0000-0002-3010-0294, http://spatialaudio.net/author/frank-schultz/, https://www.researchgate.net/profile/Frank_Schultz

developed for [Schultz2016]

```
#!bib
@PhdThesis{Schultz2016,
  Title                    = {Sound Field Synthesis for Line Source Array Applications in Large-Scale Sound Reinforcement},
  Author                   = {Frank Schultz},
  School                   = {University of Rostock},
  Year                     = {2016},
  Note                     = {\url{http://rosdok.uni-rostock.de/resolve/urn/urn:nbn:de:gbv:28-diss2016-0078-1}, URN: urn:nbn:de:gbv:28-diss2016-0078-1}
}
```

![SPLxy_Slides_Teaser.jpg](https://bitbucket.org/repo/G4rokn/images/4217367580-SPLxy_Slides_Teaser.jpg)