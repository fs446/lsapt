actpath = cd;                           %get the path where this script was called from -> this is our root folder
addpath(genpath(actpath),'-begin')      %get all subfolders included

run_for_the_first_time = exist('fh1','var');  %check if the handler for figure #1 is existing already, if not
if run_for_the_first_time==0            %we assume that we have to open the figures and init the figure 
                                        %handles, as this might be the very first start of the script  
    run getFigureHandles                %for the actual running matlab instance
end
clear all;                              %clear ram and screen, figures remain open, handles for the figures will be cleared
load fighandles;                        %reload the figure handles
clc;                                    %clear screen

%use cbrewer colormap for surf plots:
CM_tmp = {'Blues','BuGn','BuPu','GnBu','Greens','Greys','Oranges','OrRd','PuBu','PuBuGn','PuRd',...
             'Purples','RdPu', 'Reds', 'YlGn', 'YlGnBu', 'YlOrBr', 'YlOrRd'};
CMType = 'seq';
CM = CM_tmp{16}; %YlGnBu

disp('************************************')
disp('Line Source Array Prediction Toolbox')
disp('https://fs446@bitbucket.org/fs446/lsapt.git')
disp('************************************')

% ToDos:
% PlotParam f�r LB1, LB2, Avoid
% Audience Diskretisierung equi
% DI in Fkt.
% Phase Alignment PAud
% Wellenfront plotten
% 10 waveguides vs. 1 waveguide postfilter
% front vs. back hinge
% Filter per Driver als SOS -> Dirac -> FIR
% PEQs f�r individual cabinets TBD
% plot not valid region in SPLxy and FRP
% split user input and calcs
% save setup->nur noch driver functionen basteln
% CBT als Point src mit Fenster
% t into Ac.t!!!